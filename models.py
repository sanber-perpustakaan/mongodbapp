from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument

connection = connect(db="perpustakaan", host="localhost", port=27017)

if connection:
    print("MongoDB Connected")


class books(Document):
    nama = StringField(required=True, max_length=255)
    pengarang = StringField(required=True, max_length=70)
    genre = StringField(required=True, max_length=20)
    tahunterbit = StringField(required=True, max_length=20)
