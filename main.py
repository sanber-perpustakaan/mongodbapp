from models import books
import pprint as p
from bson.objectid import ObjectId
import csv


def showBooks():
    try:
        allbookdata = books.objects()
        for doc in allbookdata:
            print(doc.nama, doc.pengarang, doc.tahunterbit, doc.genre)
            # print(allbookdata.to_json())
    except Exception as e:
        print(e)


def showBookById(bookId):
    try:
        result = books.objects(id=ObjectId(bookId)).first()
        print(result.nama, result.pengarang, result.tahunterbit, result.genre)
        # print(result.to_json())
    except Exception as e:
        print(e)


def insertBooks():
    bookdata = open("bestsellers-with-categories.csv", encoding='utf-8')
    allbooks = csv.reader(bookdata, delimiter=',')
    next(allbooks)
    for book in allbooks:
        data = {
            "nama": book[0],
            "pengarang": book[1],
            "tahunterbit": book[5],
            "genre": book[6]
        }
        books(**data).save()


def updateBookById(bookId, newVal):
    try:
        result = books.objects(id=ObjectId(bookId)).first()
        result.genre = newVal
        result.save()
    except Exception as e:
        print(e)


def deleteBookById(bookId):
    try:
        result = books.objects(id=ObjectId(bookId)).first()
        result.delete()
    except Exception as e:
        print(e)


if __name__ == '__main__':
    # insertBooks()
    # showBooks()
    #updateBookById("606f050571535f54a7e975e0", "Science Fiction")
    showBookById("606f050571535f54a7e975e0")
    # deleteBookById("606f050571535f54a7e975e0")
